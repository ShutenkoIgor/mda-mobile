﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppsFlyerSendEvent : MonoBehaviour
{

    private void Start()
    {
        AppsFlyer.setAppsFlyerKey("zvfbQEJ6xFqxiVjQvonAfA");
#if UNITY_IOS
   AppsFlyer.setAppID ("YOUR_APP_ID_HERE");
   AppsFlyer.trackAppLaunch ();
#elif UNITY_ANDROID
        AppsFlyer.setAppID("com.Igor.Shutenko");
        AppsFlyer.init("zvfbQEJ6xFqxiVjQvonAfA", "AppsFlyerTrackerCallbacks");
#endif

        Invoke("SendMessage", 2.0f);
    }

    private void SendMessage()
    {
        AppsFlyer.trackRichEvent("I am working",null);
    }
}
