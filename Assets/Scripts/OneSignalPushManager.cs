﻿using UnityEngine;
using System.Collections.Generic;
using OneSignalPush.MiniJSON;

public class OneSignalPushManager : MonoBehaviour
{
    [SerializeField] private string oneSignalAppID = "fc87646b-50ab-4b3c-96a6-2d2c360a75e9";

    private static string extraMessage;
    private static bool requiresUserPrivacyConsent = false;

    void Start()
    {
        extraMessage = null;
        OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.SetRequiresUserPrivacyConsent(requiresUserPrivacyConsent);

        OneSignal.StartInit(oneSignalAppID)
                 .HandleNotificationReceived(HandleNotificationReceived)
                 .HandleNotificationOpened(HandleNotificationOpened)
                 .EndInit();

        OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;
        OneSignal.permissionObserver += OneSignal_permissionObserver;
        OneSignal.subscriptionObserver += OneSignal_subscriptionObserver;
        OneSignal.emailSubscriptionObserver += OneSignal_emailSubscriptionObserver;

        var pushState = OneSignal.GetPermissionSubscriptionState();
    }

    private void OneSignal_subscriptionObserver(OSSubscriptionStateChanges stateChanges)
    {
        Debug.Log("SUBSCRIPTION stateChanges: " + stateChanges);
        Debug.Log("SUBSCRIPTION stateChanges.to.userId: " + stateChanges.to.userId);
        Debug.Log("SUBSCRIPTION stateChanges.to.subscribed: " + stateChanges.to.subscribed);
    }

    private void OneSignal_permissionObserver(OSPermissionStateChanges stateChanges)
    {
        Debug.Log("PERMISSION stateChanges.from.status: " + stateChanges.from.status);
        Debug.Log("PERMISSION stateChanges.to.status: " + stateChanges.to.status);
    }

    private void OneSignal_emailSubscriptionObserver(OSEmailSubscriptionStateChanges stateChanges)
    {
        Debug.Log("EMAIL stateChanges.from.status: " + stateChanges.from.emailUserId + ", " + stateChanges.from.emailAddress);
        Debug.Log("EMAIL stateChanges.to.status: " + stateChanges.to.emailUserId + ", " + stateChanges.to.emailAddress);
    }

    private static void HandleNotificationReceived(OSNotification notification)
    {
        OSNotificationPayload payload = notification.payload;
        string message = payload.body;

        print("GameControllerExample:HandleNotificationReceived: " + message);
        print("displayType: " + notification.displayType);
        extraMessage = "Notification received with text: " + message;

        Dictionary<string, object> additionalData = payload.additionalData;
        if (additionalData == null)
            Debug.Log("[HandleNotificationReceived] Additional Data == null");
        else
            Debug.Log("[HandleNotificationReceived] message " + message + ", additionalData: " + Json.Serialize(additionalData) as string);
    }

    public static void HandleNotificationOpened(OSNotificationOpenedResult result)
    {
        OSNotificationPayload payload = result.notification.payload;
        string message = payload.body;
        string actionID = result.action.actionID;

        print("GameControllerExample:HandleNotificationOpened: " + message);
        extraMessage = "Notification opened with text: " + message;

        Dictionary<string, object> additionalData = payload.additionalData;
        if (additionalData == null)
            Debug.Log("[HandleNotificationOpened] Additional Data == null");
        else
            Debug.Log("[HandleNotificationOpened] message " + message + ", additionalData: " + Json.Serialize(additionalData) as string);

        if (actionID != null)
        {
            extraMessage = "Pressed ButtonId: " + actionID;
        }
    }
}
