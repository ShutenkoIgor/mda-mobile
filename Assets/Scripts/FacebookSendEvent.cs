﻿using UnityEngine;
using Facebook.Unity;

public class FacebookSendEvent : MonoBehaviour
{
    void Awake()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            FB.Init(() => {
                FB.ActivateApp();
            });
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                FB.Init(() => {
                    FB.ActivateApp();
                });
            }
        }
    }

    private void Start()
    {
        Invoke("SendEvent", 2.0f);
    }

    private void SendEvent()
    {
        FB.LogAppEvent("I am working");
    }

}
