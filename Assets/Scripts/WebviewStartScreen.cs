﻿using UnityEngine;

public class WebviewStartScreen : MonoBehaviour
{
    [SerializeField] private string defaultUrl = "https://google.com";
    [SerializeField] private UniWebView webView;

    private void Awake()
    {
        WebViewInit();
    }

    private void WebViewInit()
    {
        webView.OnLoadComplete += OnLoadComplete;
        webView.InsetsForScreenOreitation += InsetsForScreenOreitation;

        LoadPage(defaultUrl);
    }

    public void LoadPage(string _url)
    {
        webView.url = _url;
        webView.Load();
    }

    void OnLoadComplete(UniWebView webView, bool success, string errorMessage)
    {
        if (success)
            webView.Show();
        else
            Debug.Log("Something wrong in webview loading: " + errorMessage);
    }

    UniWebViewEdgeInsets InsetsForScreenOreitation(UniWebView webView, UniWebViewOrientation orientation)
    {
        if (orientation == UniWebViewOrientation.Portrait)
            return new UniWebViewEdgeInsets(5, 5, 5, 5);
        else
            return new UniWebViewEdgeInsets(5, 5, 5, 5);
    }
}
