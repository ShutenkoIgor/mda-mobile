﻿using Facebook.Unity;
using System.Collections.Generic;
using UnityEngine;

public class DeepLink : MonoBehaviour
{
    [SerializeField] private WebviewStartScreen startScreen;
    [SerializeField] private List<Link> links = new List<Link>();

    void Awake()
    {
        if (!FB.IsInitialized)
            FB.Init(InitCallback);
        else
            InitCallback();
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
            FB.ActivateApp();
        else
            Debug.Log("Failed to Initialize the Facebook SDK");
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                FB.Init(() => {
                    FB.ActivateApp();
                });
            }
        }
    }

    private void Start()
    {
        Invoke("GetDeepLink",1.0f);
    }

    private void GetDeepLink() => FB.GetAppLink(DeepLinkCallback);

    private void DeepLinkCallback(IAppLinkResult result)
    {
        if (!System.String.IsNullOrEmpty(result.TargetUrl))
        {
            int index;
            foreach(Link link in links)
            {
                index = (new System.Uri(result.TargetUrl)).Query.IndexOf(link.metka);
                if (index != -1)
                {
                    startScreen.LoadPage(link.url);
                    break;
                }
            }
        }
    }

   [System.Serializable]
   public class Link
    {
        public string metka;
        public string url;
    }
}